import sys, os

def getSizeAndBiggestFile(path_to_dir):
	dir_size = 0 #total size of given directory
	max_size = 0 #size of the biggest file in given directory
	content = os.listdir(path_to_dir) #all files and directories in given directory
	for f in content:
		if os.path.isdir(path_to_dir+"/"+f): #chcecking if f is a directory
			size, b_file, b_file_size = getSizeAndBiggestFile(path_to_dir+"/"+f) #recursive call of the function for subdirectory returning information about that subdirectory
			dir_size += size #add total size of subdirectory f to total size of directory
			if b_file_size > max_size: #checking if the biggest file in subdirectory f is bigger than current biggest file
				max_size = b_file_size
				biggest_file = b_file
		else: #if f is not a directory
			statinfo = os.stat(path_to_dir+"/"+f) #getting information about file f
			dir_size += statinfo.st_size #add size of file f to total size of directory
			if statinfo.st_size > max_size: #checking if file f is bigger than current biggest file
				max_size = statinfo.st_size
				biggest_file = f
	return dir_size, biggest_file, max_size #returning total size of given directory, name of the biggest file and its size

# path to directory given as a command line argument
path = sys.argv[1]

result = getSizeAndBiggestFile(path)
print("size of directory: " + str(result[0]))
print("the biggest file: " + str(result[1]))
print("name of the biggest file: " + str(result[2]))