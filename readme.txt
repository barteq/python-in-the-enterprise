directory.py v. 1.0 

The script prints total size of given directory, name of the biggest file in that directory and its size.

To run the script type:
python directory.py <path_to_directory>